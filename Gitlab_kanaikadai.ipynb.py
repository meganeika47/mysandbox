#!/usr/bin/env python
# coding: utf-8

# # このノートブックは、Google Colabで作成したものをGitlab連携をするけど、**金井くんの課題用です。**
#  
# 1.   Gitlabのdigi-pythonリポジトリのファイルたちを自分のGoogleDriveに持ってくる＝"Clone"
# 2.   Gitlabのsandboxリポジトリへ、自分の作成物を追加する＝"Add、commit、push"
#  
# # パスワード直書きをしないといけないので、**必ず各人の、ほかの人には参照できないGoogleDriveのなかにコピーして、自分用に保管してください！**

# # まず、GoogleDriveをマウントします。

# いろいろ方法はあるけど一番簡単なのは以下。
# ![GoogleDriveをマウント１](https://qiita-user-contents.imgix.net/https%3A%2F%2Fqiita-image-store.s3.ap-northeast-1.amazonaws.com%2F0%2F187186%2F01c80e7c-e1b0-ccec-9fcd-2810fb42e96d.png?ixlib=rb-1.2.2&auto=format&gif-q=60&q=75&s=a5ccdb24c4a5b6090f6f5cf5189cb64e)
# ![GoogleDriveをマウント１](https://qiita-user-contents.imgix.net/https%3A%2F%2Fqiita-image-store.s3.ap-northeast-1.amazonaws.com%2F0%2F187186%2Fac924503-8126-5ec6-45ab-be5525354760.png?ixlib=rb-1.2.2&auto=format&gif-q=60&q=75&w=1400&fit=max&s=0c6ea8d74afc0d424f725c685371b6ad)

# # 【初回のみ】出てきた　drive/My Drive/の下に、「MyKadai」フォルダを新しく作成します。

# 任意のフォルダ名でもいいのですが、ここより先の説明はこのフォルダ名を前提に進めます。

# In[ ]:


get_ipython().magic(u'mkdir "/content/drive/My Drive/MyKadai"')


# #「MyKadai」フォルダに、ハッカソンプロジェクトのデジ推課題リポジトリのCloneを作成します。

# 遠いクラウド上に置いてあるものを、お手元のGoogleDriveに引っ張ってくるよってイメージです。

# In[ ]:


get_ipython().magic(u'cd "/content/drive/My Drive/MyKadai"')
get_ipython().system(u'git clone https://gitlab.com/hackathon_lite_SKFRONT/digi-python.git')
get_ipython().magic(u'cd digi-python')
get_ipython().magic(u'ls -l')


# #作成物ができたら、MyKadaiフォルダへ作成物をコピーしてから、リポジトリへの反映をします。

# user.name、user.emailはご自分のものを指定してください。

# In[ ]:


get_ipython().system(u'git config --global user.name "Manami.Iizuka"')
get_ipython().system(u'git config --global user.email "meganeika47@gmail.com"')


# 1.  作成物をClone先にコピーします。作成物のフルパスは、適宜書き換えてください。（さきほどcdしたので、MyKadai/digi-pythonがカレントのはず）
#  
# あとはgitコマンド。
#  
# 2.   カレントの作成物をAdd
# 3.   コメントを添えてcommit。コメントには「LITE-26」など、JIRAの識別子と紐づけると良い
# 4.   認証のためのおまじない。ここがパスワード直書きです！管理要注意！
#  
# ｈｔｔｐｓ：//<下記図の赤枠>：<Gitlabのパスワード>@gitlab.com/hackathon_lite_SKFRONT/digi-python.git
#  
# 赤枠＝飯塚はGmailの@より左ですが、ひとそれぞれのようです
#  
# ![赤枠ね](https://gitlab.com/hackathon_lite_SKFRONT/sandbox/-/raw/master/sc1.png)
#  
#  
#  
# 5.   リポジトリへの格納

# In[ ]:


get_ipython().system(u'cp "/content/drive/My Drive/Colab Notebooks/\u4f5c\u3063\u305f\u3082\u306e.ipynb" ./')
get_ipython().system(u'git add \u4f5c\u3063\u305f\u3082\u306e.ipynb')
get_ipython().system(u'git commit --message="LITE-26 \u3068\u308a\u3042\u3048\u305a\u4f5c\u3063\u3066\u307f\u305f\u3000\u3068\u304b\u305d\u3046\u3044\u3046\u611f\u3058\u3067\u30b3\u30e1\u30f3\u30c8"')
#<下記図の赤枠>、<Gitlabのパスワード>　は必ず自分用に書き換えてくださいね
get_ipython().system(u'git remote set-url origin https://<\u4e0b\u8a18\u56f3\u306e\u8d64\u67a0>:<Gitlab\u306e\u30d1\u30b9\u30ef\u30fc\u30c9>@gitlab.com/hackathon_lite_SKFRONT/digi-python.git')
get_ipython().system(u'git push')


# # 最後に、ちゃんとリポジトリに反映されているか、見てみましょう。

# [ハッカソンlite_証券フロント合同チーム＞デジ推課題](https://gitlab.com/hackathon_lite_SKFRONT/digi-python)

# うまくいかなかったら飯塚までご連絡を！スクショとか取っといてくれると嬉しいです！
